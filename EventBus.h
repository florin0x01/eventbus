#ifndef EVENTBUS_EVENTBUS_H
#define EVENTBUS_EVENTBUS_H

#include <map>
#include <string>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <iostream>

// A1: 1 1 0 2 3 4
// A2: 1 1 1

namespace StoicEventBus {
    class EventBusCancellationException : public std::exception {
    private:
        std::string errMsg;
    public:
        EventBusCancellationException(const std::string &msg) : errMsg{msg} {};

        virtual char const *what() const noexcept {
            return errMsg.c_str();
        }
    };

    class EventStopInvokingAfterXException: public std::exception {
    public:
        int cnt;
        EventStopInvokingAfterXException(int howMany=0): cnt(howMany)
        {

        }
    };

    template<typename T, typename F, typename ...Args>
    class EventBus {
        using holdingstructure = std::multimap<T, F>;

    public:
        EventBus() {
            lastPos = map.end();
        }

        void add(T &&name, F &&f) {
            const std::lock_guard<std::mutex> lock(mutex);
            map.insert({std::forward<T>(name), std::forward<F>(f)});
        }

        void invoke(T &&key, Args &&...args)
        {
            //const std::shared_lock<std::shared_mutex> lock(mutex);
            const std::lock_guard<std::mutex> lock(mutex);
            auto values = map.equal_range(std::forward<T>(key));
        //    std::cout << "<INVOKE> " << key << std::endl;

            auto lambdaCall = [count=-1](auto f, const T &key) mutable {
                const int LAMBDAINVOKE_DEFAULT_RETURNVAL = -500;
                if (count > 0)  {
                    f();
                    return --count;
                }
                else if (count == 0) {
                    throw EventBusCancellationException(std::string("Scheduled cancellation: No more functions to call for key ").append(key));
                } else {
                    try {
                        f();
                    }catch (EventStopInvokingAfterXException &e) {
                        count = e.cnt;
                        return count;
                    }
                    return LAMBDAINVOKE_DEFAULT_RETURNVAL;
                }
            };

            for (auto it = values.first; it != values.second; ++it) {
              auto bindFn = std::bind(it->second, args ...);
              lambdaCall(bindFn, key);
            }
        }

    private:
        typename holdingstructure::const_iterator lastPos;
        holdingstructure map;
        std::mutex mutex;
    };
}


#endif //EVENTBUS_EVENTBUS_H
