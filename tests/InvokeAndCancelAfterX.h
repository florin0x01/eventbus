//
// Created by Fl on 14/02/2020.
//

#ifndef EVENTBUS_INVOKEANDCANCELAFTERX_H
#define EVENTBUS_INVOKEANDCANCELAFTERX_H


#include <iostream>
#include "tests/TestCase.h"

namespace StoicEventBus {
    namespace tests {

        class InvokeAndCancelAfterX : public TestCase {
        public:
            InvokeAndCancelAfterX(const std::string &name) : TestCase(name) {

            }

            virtual void run(TestParams) override;
        };

    }
}


#endif //EVENTBUS_INVOKEANDCANCELAFTERX_H
