#ifndef EVENTBUS_THREADCASE1_H
#define EVENTBUS_THREADCASE1_H

#include "tests/InvokeAndCancel.h"
#include "EventBus.h"
#include "utils/time_utils.h"
#include "TestCase.h"


namespace StoicEventBus {
    namespace tests {

        class ThreadCase1 : public TestCase {
        public:
            ThreadCase1(const std::string &name) : TestCase(name) {

            }

            virtual void run(TestParams) override;
        };
    }
}


#endif //EVENTBUS_THREADCASE1_H
