#ifndef EVENTBUS_TESTFACTORY_H
#define EVENTBUS_TESTFACTORY_H

#include <memory>
#include "tests/TestCase.h"

namespace StoicEventBus {
    namespace tests {
        enum class TestCaseNames {
            SameKeyInsertAndInvoke,
            SameKeyInsertInvokeCancelAndContinue,
            InvokeAndCancelAfterX,
            Thread1
        };

        class TestFactory {
        public:
            TestFactory() = delete;

            TestFactory(const TestFactory &) = delete;

            TestFactory(TestFactory &&) = delete;

            TestFactory &operator=(const TestFactory &) = delete;

            static std::unique_ptr<TestCase> fetchTest(const TestCaseNames t);
        };
    }
}

#endif //EVENTBUS_TESTFACTORY_H
