#ifndef EVENTBUS_TESTCASE_H
#define EVENTBUS_TESTCASE_H

#include "EventBus.h"
#include <string>

namespace StoicEventBus{
    namespace tests {

        struct TestParams {
            uint32_t insertions;
        };

        class TestCase {
        public:
            const std::string &name;
            TestCase(const std::string &name) : name(name) {}
            virtual void run(TestParams params) = 0;
            virtual ~TestCase() {}
        };
    }
}


#endif //EVENTBUS_TESTCASE_H
