#include "tests/InvokeAndCancel.h"
#include "EventBus.h"
#include "utils/time_utils.h"
#include "TestCase.h"
#include <iostream>
#include <chrono>
#include <random>


using BusFunction1 =  std::function<void(const std::string)>;
using StringFunctionsBus =  StoicEventBus::EventBus<std::string, BusFunction1, std::string>;

static void ExecuteAttachmentWithCancel(std::string x) {
    std::cout << "ExecuteAttachmentWithCancel " << x << std::endl;
    throw StoicEventBus::EventBusCancellationException("Cancelling event");
}

static void ExecuteAttachment2(std::string x) {
    std::cout << "ExecuteAttachment2 " << x << std::endl;
}

static void ExecuteAttachment3(std::string x) {
    std::cout << "ExecuteAttachment3 " << x << std::endl;
}

static void addToBus(StringFunctionsBus &bus, int index, BusFunction1 fn) {
    auto generatedFnName = std::string("Name") + std::to_string(index);
    bus.add(std::move(generatedFnName), std::move(fn));
}

static void callBus(StringFunctionsBus &bus, int index) {
    // std::cout << "[THR ]" << index << "INVOKE " name << std::endl;
    auto generatedFnName = std::string("Name") + std::to_string(index);
    bus.invoke(std::move(generatedFnName), std::string("INVOKEPARAM ") + std::to_string(index));
}

static void fn(int i) {
    std::cout << "Hello " << i << std::endl;
}

void StoicEventBus::tests::InvokeAndCancel::run(StoicEventBus::tests::TestParams params) {
    std::cout << "Running " << __FILE__ << std::endl;

    StringFunctionsBus bus;
    auto t1 = std::chrono::steady_clock::now();

    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    auto index_pos_fn_to_fail = std::bind(std::uniform_int_distribution<uint32_t>(1, params.insertions / 2),
                                          std::mt19937(seed));


    auto indexPosToFailN = index_pos_fn_to_fail();

    std::cout << "RNG: " << indexPosToFailN << std::endl;


    for (auto i = 0; i < params.insertions; i++) {
        if (i % 10 == 0) {
            bus.add("AddAttachment", [i](std::string x = "") {
                //     std::cout << " Lambda AddAttachment Attachment [" << i << "] " << x << std::endl;
            });
        }
        if (i % 50 == 0) {
            bus.add("ReadAttachment", [i](std::string x = "") {
                //       std::cout << " Lambda ReadAttachment Attachment [" << i << "] " << x << std::endl;
            });
        }
        if (i % 100 == 0) {
            bus.add("RemoveAttachment", [i](std::string x = "") {
                //  std::cout << "Lambda RemoveAttachment Attachment [" << i << "] " << x << std::endl;
            });
        }

        if (i == indexPosToFailN) {
            bus.add("ReadAttachment", [i](std::string x = "") {
                std::cout << "Next ReadAttachments will not be executed. Breaking at [" << i << " ] " << x << std::endl;
               // throw StoicEventBus::EventBusCancellationException("No more ReadAttachments after this one");
               throw StoicEventBus::EventStopInvokingAfterXException(4);
            });
        }
    }

    auto t2 = std::chrono::steady_clock::now();

    auto d_nano = std::chrono::duration_cast<nano_s>(t2 - t1).count();
    auto d_micro = std::chrono::duration_cast<micro_s>(t2 - t1).count();
    auto d_milli = std::chrono::duration_cast<milli_s>(t2 - t1).count();
    auto d_s = std::chrono::duration_cast<seconds>(t2 - t1).count();
    auto d_m = std::chrono::duration_cast<minutes>(t2 - t1).count();
    auto d_h = std::chrono::duration_cast<hours>(t2 - t1).count();

    std::cout << "BUS ADD d_nano:   " << d_nano << "\n"
              << "d_micro:  " << d_micro << "\n"
              << "d_milli:  " << d_milli << "\n"
              << "d_s:      " << d_s << "\n"
              << "d_m:      " << d_m << "\n"
              << "d_h:      " << d_h << "\n"
              << std::endl;

    t1 = std::chrono::steady_clock::now();

    try {
        std::cout << "Invoking ReadAttachment" << std::endl;
        bus.invoke("ReadAttachment", "path/to/attachment");
    }  catch(StoicEventBus::EventBusCancellationException &e2) {
        std::cout << "Got EventBusCancellationException" << e2.what() << std::endl;
    }

    t2 = std::chrono::steady_clock::now();

 //   bus.invoke("ReadAttachment", "/path/to/attachment");
    std::cout << "Invoking AddAttachment " << std::endl;
    bus.invoke("AddAttachment", "/path/to/attachment");

    d_nano = std::chrono::duration_cast<nano_s>(t2 - t1).count();
    d_micro = std::chrono::duration_cast<micro_s>(t2 - t1).count();
    d_milli = std::chrono::duration_cast<milli_s>(t2 - t1).count();
    d_s = std::chrono::duration_cast<seconds>(t2 - t1).count();
    d_m = std::chrono::duration_cast<minutes>(t2 - t1).count();
    d_h = std::chrono::duration_cast<hours>(t2 - t1).count();

    std::cout << "BUS INVOKE for ReadAttachment d_nano:   " << d_nano << "\n"
              << "d_micro:  " << d_micro << "\n"
              << "d_milli:  " << d_milli << "\n"
              << "d_s:      " << d_s << "\n"
              << "d_m:      " << d_m << "\n"
              << "d_h:      " << d_h << "\n"
              << std::endl;


    std::cout << "Hello Simple";
}