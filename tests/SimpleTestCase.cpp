#include "tests/SimpleTestCase.h"
#include "EventBus.h"
#include "utils/time_utils.h"
#include <iostream>
#include <chrono>

using BusFunction1 =  std::function<void(const std::string)>;
using StringFunctionsBus =  StoicEventBus::EventBus<std::string, BusFunction1, std::string>;

static void ExecuteAttachmentWithCancel(std::string x) {
    std::cout << "ExecuteAttachmentWithCancel " << x << std::endl;
    throw StoicEventBus::EventBusCancellationException("Cancelling event");
}

static void ExecuteAttachment2(std::string x) {
    std::cout << "ExecuteAttachment2 " << x << std::endl;
}

static void ExecuteAttachment3(std::string x) {
    std::cout << "ExecuteAttachment3 " << x << std::endl;
}

static void addToBus(StringFunctionsBus &bus, int index, BusFunction1 fn) {
    auto generatedFnName = std::string("Name") + std::to_string(index);
    bus.add(std::move(generatedFnName), std::move(fn));
}

static void callBus(StringFunctionsBus &bus, int index) {
    // std::cout << "[THR ]" << index << "INVOKE " name << std::endl;
    auto generatedFnName = std::string("Name") + std::to_string(index);
    bus.invoke(std::move(generatedFnName), std::string("INVOKEPARAM ") + std::to_string(index));
}

static void fn(int i) {
    std::cout << "Hello " << i << std::endl;
}

void StoicEventBus::tests::SimpleTestCase::run(TestParams params) {
    std::cout << "Running " << __FILE__ << std::endl;

    StringFunctionsBus bus;
    auto t1 = std::chrono::steady_clock::now();
    for (auto i = 0; i < params.insertions; i++) {
        bus.add("AddAttachment", [i](std::string x = "") {
            //std::cout << "Lambda Attachment [" << i << "] " x << "\n";
        });

//    if (i % 10 == 0) {
//     //   std::cout << "ADD 1";
//        bus.add("AddAttachment", [i](std::string x = "") {
//            //std::cout << "Lambda Attachment [" << i << "] " x << "\n";
//        });
//    }   if ( i % 50 == 0) {
//       // std::cout << "ADD 2";
//
//        bus.add("ReadAttachment", [i](std::string x = "") {
//            //std::cout << "Lambda Attachment [" << i << "] " x << "\n";
//        });
//    }  if ( i % 100 == 0) {
//       // std::cout << "ADD 3";
//
//        bus.add("RemoveAttachment", [i](std::string x = "") {
//            //std::cout << "Lambda Attachment [" << i << "] " x << "\n";
//        });
//    }
    }
    auto t2 = std::chrono::steady_clock::now();

    auto d_nano = std::chrono::duration_cast<nano_s>(t2 - t1).count();
    auto d_micro = std::chrono::duration_cast<micro_s>(t2 - t1).count();
    auto d_milli = std::chrono::duration_cast<milli_s>(t2 - t1).count();
    auto d_s = std::chrono::duration_cast<seconds>(t2 - t1).count();
    auto d_m = std::chrono::duration_cast<minutes>(t2 - t1).count();
    auto d_h = std::chrono::duration_cast<hours>(t2 - t1).count();

    std::cout << "BUS ADD d_nano:   " << d_nano << "\n"
              << "d_micro:  " << d_micro << "\n"
              << "d_milli:  " << d_milli << "\n"
              << "d_s:      " << d_s << "\n"
              << "d_m:      " << d_m << "\n"
              << "d_h:      " << d_h << "\n"
              << std::endl;

    t1 = std::chrono::steady_clock::now();

    try {
         bus.invoke("AddAttachment", "path/to/attachment");
    } catch (StoicEventBus::EventBusCancellationException &e) {
        std::cout << "Got cancellation exception: " << e.what() << std::endl;
    }
    t2 = std::chrono::steady_clock::now();

    d_nano = std::chrono::duration_cast<nano_s>(t2 - t1).count();
    d_micro = std::chrono::duration_cast<micro_s>(t2 - t1).count();
    d_milli = std::chrono::duration_cast<milli_s>(t2 - t1).count();
    d_s = std::chrono::duration_cast<seconds>(t2 - t1).count();
    d_m = std::chrono::duration_cast<minutes>(t2 - t1).count();
    d_h = std::chrono::duration_cast<hours>(t2 - t1).count();

    std::cout << "BUS INVOKE d_nano:   " << d_nano << "\n"
              << "d_micro:  " << d_micro << "\n"
              << "d_milli:  " << d_milli << "\n"
              << "d_s:      " << d_s << "\n"
              << "d_m:      " << d_m << "\n"
              << "d_h:      " << d_h << "\n"
              << std::endl;


    std::cout << "Hello Simple";
}