#ifndef EVENTBUS_INVOKEANDCANCEL_H
#define EVENTBUS_INVOKEANDCANCEL_H

#include <iostream>
#include "tests/TestCase.h"

namespace StoicEventBus {
    namespace tests {

        class InvokeAndCancel : public TestCase {
        public:
            InvokeAndCancel(const std::string &name) : TestCase(name) {

            }

            virtual void run(TestParams) override;
        };

    }
}


#endif //EVENTBUS_INVOKEANDCANCEL_H
