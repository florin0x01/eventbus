#ifndef EVENTBUS_SIMPLETESTCASE_H
#define EVENTBUS_SIMPLETESTCASE_H

#include <iostream>
#include "tests/TestCase.h"

namespace StoicEventBus {
    namespace tests {
        class SimpleTestCase : public TestCase {
        public:
            SimpleTestCase(const std::string &name) : TestCase(name) {

            }

            virtual void run(TestParams) override;
        };
    }
}

#endif //EVENTBUS_SIMPLETESTCASE_H
