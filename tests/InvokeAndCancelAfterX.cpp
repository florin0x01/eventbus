#include "tests/InvokeAndCancelAfterX.h"
#include "EventBus.h"
#include "utils/time_utils.h"
#include "TestCase.h"
#include <iostream>
#include <chrono>
#include <random>


using BusFunction1 =  std::function<void(const std::string)>;
using StringFunctionsBus =  StoicEventBus::EventBus<std::string, BusFunction1, std::string>;

static void AddAttachment1(std::string x) {
    std::cout << "AddAttachment1 " << x << std::endl;
}

static void AddAttachment2(std::string x) {
    std::cout << "AddAttachment2 " << x << std::endl;
}

static void AddAttachment3(std::string x) {
    std::cout << "AddAttachment3 " << x << std::endl;
}

static void AddAttachment4(std::string x) {
    std::cout << "AddAttachment4 " << x << std::endl;
}

static void AddAttachment5(std::string x) {
    std::cout << "AddAttachment5 " << x << std::endl;
}

static void AddAttachment6(std::string x) {
    std::cout << "AddAttachment6 " << x << std::endl;
}

static void AddAttachment7(std::string x) {
    std::cout << "AddAttachment7 " << x << std::endl;
}

static void ExecuteAttachmentWithCancel(std::string x) {
    std::cout << "ExecuteAttachmentWithCancel " << x << std::endl;
    throw StoicEventBus::EventStopInvokingAfterXException(0);
   // throw StoicEventBus::EventBusCancellationException("CANCEL");
}

static void ReadAttachment(std::string x) {
    std::cout << "ReadAttachment " << x << std::endl;

}

void StoicEventBus::tests::InvokeAndCancelAfterX::run(StoicEventBus::tests::TestParams params) {
    std::cout << "Running " << __FILE__ << std::endl;

    StringFunctionsBus bus;
    auto t1 = std::chrono::steady_clock::now();

    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();

    bus.add("AddAttachment", AddAttachment1);
    bus.add("AddAttachment", AddAttachment2);
    bus.add("AddAttachment", ExecuteAttachmentWithCancel);
    bus.add("AddAttachment", AddAttachment3);
    bus.add("AddAttachment", AddAttachment4);
    bus.add("AddAttachment", AddAttachment5);
    bus.add("AddAttachment", AddAttachment6);
    bus.add("AddAttachment", AddAttachment7);


    bus.add("ReadAttachment", ReadAttachment);

    try {
        bus.invoke("AddAttachment", "/path.1");
    }catch(StoicEventBus::EventBusCancellationException &ex) {
        std::cout << ex.what() << std::endl;
    }
    bus.invoke("ReadAttachment", "/path.2");
}