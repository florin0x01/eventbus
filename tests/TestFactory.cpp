#include "tests/TestFactory.h"
#include "tests/TestCase.h"
#include "tests/InvokeAndCancel.h"
#include "tests/SimpleTestCase.h"
#include "tests/InvokeAndCancelAfterX.h"
#include "ThreadCase1.h"
#include <memory>
#include <string>


using namespace StoicEventBus::tests;

std::unique_ptr<TestCase> TestFactory::fetchTest(const TestCaseNames t) {
    switch (t) {
        case TestCaseNames::SameKeyInsertAndInvoke: {
            return std::make_unique<SimpleTestCase>("SameKeyInsertAndInvoke");
        }
        case TestCaseNames::SameKeyInsertInvokeCancelAndContinue: {
            return std::make_unique<InvokeAndCancel>("SameKeyInsertInvokeCancelAndContinue");
        }
        case TestCaseNames::InvokeAndCancelAfterX: {
            return std::make_unique<InvokeAndCancelAfterX>("InvokeAndCancelAfterX");
        }
        case TestCaseNames::Thread1: {
            return std::make_unique<ThreadCase1>("ThreadCase1");
        }
    }
}

