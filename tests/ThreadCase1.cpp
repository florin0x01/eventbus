//
// Created by Fl on 15/02/2020.
//

#include <iostream>
#include <chrono>
#include <random>
#include <string>
#include <sstream>
#include "tests/ThreadCase1.h"
#include "EventBus.h"
#include "utils/time_utils.h"
#include "TestCase.h"


//TODO use shared_lock / shared_mutex for concurrent read acc??

// https://www.justsoftwaresolutions.co.uk/threading/new-concurrency-features-in-c++14.html
// https://stackoverflow.com/questions/46452973/difference-between-stdmutex-and-stdshared-mutex

using BusFunction1 =  std::function<void(const std::string)>;
using StringFunctionsBus =  StoicEventBus::EventBus<std::string, BusFunction1, std::string>;

static std::string FnNameFromIndex(int index) {
    std::string generatedFnName;

    if (index % 3 == 0) {
        generatedFnName = "GenerateAttachment";
    } else if (index % 2 == 0) {
        generatedFnName = "UnzipAttachment";
    } else if (index % 5 == 0) {
        generatedFnName = "AddAttachment";
    } else if (index % 10 == 0) {
        generatedFnName = "ReadAttachment";
    } else if (index % 50 == 0) {
        generatedFnName = "DeleteAttachment";
    } else if (index % 100 == 0) {
        generatedFnName = "ProcessAttachment";
    } else {
        generatedFnName = "TearAttachment";
    }

    return generatedFnName;
}

static void addToBus(StringFunctionsBus &bus, int index, BusFunction1 fn) {
    std::string generatedFnName = FnNameFromIndex(index);

    std::stringstream ss;
    ss << "<ADD Function> " << generatedFnName << ", ID " << std::this_thread::get_id();
    std::cout << ss.str() << std::endl;

    bus.add(std::move(generatedFnName), std::move(fn));
}

static void callBus(StringFunctionsBus &bus, int index) {
    // std::cout << "[THR ]" << index << "INVOKE " name << std::endl;
    std::string generatedFnName = FnNameFromIndex(index);

    std::stringstream ss;
    ss << "<Invoke Function> " << generatedFnName << ", ID " << std::this_thread::get_id();

    try {
        bus.invoke(std::move(generatedFnName), ss.str());
    } catch(StoicEventBus::EventBusCancellationException& ex1) {
        std::cout << "CANCELLATION EXCEPTION FOR " << generatedFnName << ": " << ex1.what() << " , ID " << std::this_thread::get_id() << std::endl;
    } catch(StoicEventBus::EventStopInvokingAfterXException &ex2) {
        std::cout << "EVENTSTOPINVOKING EXCEPION FOR "<< generatedFnName << " : " << ex2.what() << ", ID " << std::this_thread::get_id() << std::endl;
    }

}

void StoicEventBus::tests::ThreadCase1::run(StoicEventBus::tests::TestParams params) {
    std::cout << "Running " << __FILE__ << std::endl;
    std::cout << "Thread id " << std::this_thread::get_id() << std::endl;

    StringFunctionsBus bus;
    int numThreads = params.insertions;
    std::thread creatingThreads[numThreads];
    std::thread callingThreads[numThreads];
    BusFunction1 busFunctions[numThreads];

    std::cout << "Numthreads " << numThreads << std::endl;

    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    auto index_pos_fn_to_fail = std::bind(std::uniform_int_distribution<uint32_t>(params.insertions/2 - 1, params.insertions - 1),
                                          std::mt19937(seed));

    auto indexPosToFailN = index_pos_fn_to_fail();

    std::cout << "RNG: " << indexPosToFailN << std::endl;

    for (auto i = 0; i < numThreads; i++) {
        busFunctions[i] = [i](std::string input) { std::cout << " FunctionBody " << FnNameFromIndex(i) << "[" << i << "], PAYLOAD: [" << input << "]" << std::endl; };
        std::cout << i << " => " << FnNameFromIndex(i) << std::endl;
    }

    for (auto i = 0; i < numThreads; i++) {
        creatingThreads[i] = std::thread(addToBus, std::ref(bus), i, busFunctions[i]);
    }
    for (auto i=0; i < numThreads; i++) {
        creatingThreads[i].join();
    }

    for (auto i=0; i < numThreads; i++) {
        callingThreads[i] = std::thread(callBus, std::ref(bus), i);
    }

    for (auto i=0; i < numThreads; i++) {
      //  creatingThreads[i].join();
        callingThreads[i].join();
    }
}



