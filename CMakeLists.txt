cmake_minimum_required(VERSION 3.15)
project(EventBus)

set(CMAKE_CXX_STANDARD 17)
find_package(Boost 1.72.0)

add_executable(EventBus
        main.cpp
        EventBus.cpp
        EventBus.h
        tests/TestFactory.cpp
        tests/TestFactory.h
        tests/SimpleTestCase.cpp
        tests/SimpleTestCase.h
        tests/TestCase.h
        tests/InvokeAndCancel.cpp
        tests/InvokeAndCancel.h
        tests/InvokeAndCancelAfterX.cpp tests/InvokeAndCancelAfterX.h tests/ThreadCase1.cpp tests/ThreadCase1.h)

if(Boost_FOUND)
    target_include_directories(EventBus PUBLIC ${Boost_INCLUDE_DIRS})
endif()
target_include_directories(EventBus PUBLIC
        ${PROJECT_SOURCE_DIR}
)
