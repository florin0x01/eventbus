#include <iostream>
#include <array>
#include <tests/TestFactory.h>

namespace stoictests = StoicEventBus::tests;

using namespace stoictests;

int main() {
    auto fn3 = [](auto x1, auto x2, ...) {};
    auto fn1 = [](std::string x = "") { std::cout << "Hello from FN1 lambda and param " << x; };
    auto fn2 = [](std::string x = "") { std::cout << "Hello fn2 " << x; };

    std::unique_ptr<TestCase> t = TestFactory::fetchTest(TestCaseNames::Thread1);
    t->run(TestParams{10});

    return 0;
}
